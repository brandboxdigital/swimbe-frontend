export const state = () => ({
    products: []
})

export const mutations = {
    setProducts(state, products) {
        state.products = products
    },
}

export const getters = {
    getProducts: state => state.products,
    getProductByVariationId: state => id => state
        .products
        .find(product => product
            .variations
            .find(variation =>
                variation.id == id
            )),
    getVariationById: (state, getters) => id => ({
        ...(state.products.find(product => product.variations.find(variation => variation.id == id)).variations.find(variation => variation.id == id)),
        product: getters.getProductByVariationId(id),
    })
}

export const actions = {
    async fetchProducts({ commit }) {
        const { data } = await this.$axios.$get("/api/v1/products/all")
        // console.log(`🩱`, data)
        commit("setProducts", data)
        return data
    }
}
