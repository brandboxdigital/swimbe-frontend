export const state = () => ({
    favorites: []
})

export const mutations = {
    setFavorites(state, favorites) {
        state.favorites = favorites
    },
}

export const getters = {
    getFavorites: state => state.favorites,
}

export const actions = {
    async fetchFavorites({ commit }) {
        const { data } = await this.$axios.$get("/api/v1/profile/favorites")

        commit("setFavorites", data)
        return data
    },
    async addFavorite({dispatch, getters}, item) {
        const favorite = await this.$axios.$post('/api/v1/profile/favorites/add', {
            variation_id: item.variation.id,
            variation_attributes: item.attributeValues,
        })
        
        if (!favorite.error) {
            dispatch('fetchFavorites')
            // commit("setFavorites", [...getters['getFavorites'], favorite.data])            
            return favorite.data?.pivot?.id || null
        }
      },
    async removeFavorite({ commit, getters }, id) {
        const favorite = await this.$axios.$post('/api/v1/profile/favorites/remove', {
          favorite_id: id,
        })

        // if (!favorite.error) {
          commit("setFavorites", getters['getFavorites'].filter((favorite) => favorite.pivot.id !== id))
        // }      
    },
}
