// import _get from "lodash/get";
import _forEach from "lodash/forEach"

export const state = () => ({
  items: [],
})

export const mutations = {

  addProduct: (state, {product, variation, count, variation_data}) => {
    state.items.push({
      product,
      variation,
      count,
      variation_data,
    });
  },

  reset: (state) => {
    state.items = [];
  },
}

export const actions = {
  ADD_PRODUCT(state, {product, variation, count, variation_data}) {
    return state.commit( 'addProduct', {
      product,
      variation,
      count,
      variation_data,
    } );
  },

  ADD_MANY_PRODUCTS(state, items) {
    const promises = [];
    _forEach(items, (item) => {
      let promise = state.commit( 'addProduct', {
        product:        item.product,
        variation:      item.variation,
        count:          item.count,
        variation_data: item.variation_data,
      } );

      promises.push(promise);
    })

    return Promise.all(promises);
  },

  RESET(state) {
    state.commit( 'reset' );
  },
}
