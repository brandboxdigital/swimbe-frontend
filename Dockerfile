# Build
FROM node:20-alpine as builder
WORKDIR /app
RUN apk --no-cache add openssh g++ make python3 git
COPY package.json /app/
RUN yarn install && yarn cache clean --force
ADD . /app
RUN yarn build

# Run
FROM node:20-slim
WORKDIR /app
COPY --from=builder /app /app
EXPOSE 3000
# ENTRYPOINT ["node", ".output/server/index.mjs"]
ENTRYPOINT ["yarn", "start"]
