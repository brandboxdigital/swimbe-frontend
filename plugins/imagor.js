import { createHmac } from 'crypto'

// Imagor hashing function for security
const sign = (path, secret) => {
  const hash = createHmac('sha1', secret)
    .update(path)
    .digest('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
  return `${hash}/${path}`
}

export default (context, inject) => {
  const compress = (data, width = '', height = '', additional = '') => {
    if (!data) return data

    // Getting url itself or "path" from object
    const url = data.path ? data.path : data

    // Checking Imagor server settings in .env file
    const IMAGOR_SERVER = process.env.IMAGOR_SERVER || "https://imagor.brandbox.digital/"
    if (!IMAGOR_SERVER || IMAGOR_SERVER==0) return url

    const isLocalhost = url.indexOf('://localhost') > -1
    if (isLocalhost) return url

    const safeAdditional = additional.substring(-1) !== '/' ? `${additional}/` : additional

    // Making path without protocol
    const urlWidthoutProtocol = url.substr(url.indexOf('://') + 3)
    const request = safeAdditional + width + 'x' + height + '/' + urlWidthoutProtocol

    // Checking if server needs hasing
    const secret = process.env.IMAGOR_SECRET

    // Returning safe or unsafe link
    return IMAGOR_SERVER + (secret ? sign(request, secret) : `unsafe/${request}`)
  }

  inject('imagor', compress)
}
