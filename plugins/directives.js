import Vue from 'vue'
import _get from "lodash/get";

Vue.directive('scroll', {
  inserted (el, binding) {
    const f = (evt) => {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})


export default (context, inject) => {

  inject('get', _get);
}
