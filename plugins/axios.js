import https from "https"

const httpsAgent = new https.Agent({ keepAlive: true })

export default function ({ $axios, app }) {
  $axios.defaults.headers.timeout = 30000

  if (!$axios.defaults.httpsAgent) {
      $axios.defaults.httpsAgent = httpsAgent
  }

  $axios.onRequest(config => {
    if (config.headers && config.headers.common) {
      config.headers.common['Accept'] = 'application/json'
      config.headers.common['Content-Type'] = 'application/json'
      config.headers.common['Accept-Language'] = app.i18n.locale
    }

    // console.log(`🟣 Making request to ${config.url}`)
  })

  $axios.onError(error => {    
    console.error("🛑 Axios", error)
  })

  $axios.onResponseError(error => {
    console.error("📛 Axios", error)
  })

  // $axios.onResponse(response => {
  //   console.error("response",response)
  //   console.error("response",$cookies.getAll({ fromRes: true }))
  //   $axios.defaults.headers.common.cookie
  //   axios.defaults.headers.common.cookie = req.headers.cookie
  // })


}
