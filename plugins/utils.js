import Vue from 'vue'
import slugify from 'slugify'
import _get from "lodash/get";

import { ToastProgrammatic as Toast } from 'buefy'

const parseCurrency = (price, count = 1, decimals = 2, currency = 'EUR') => {

  let _price = price || 0;
  let _count = count || 1;
  let _decimals = decimals;
  let _currency = currency || null;

  // If price is money object
  if (typeof price === "object") {
    _price = price.amount;
    _currency = price.currency;
  }

  const amount = _price * _count / 100;

  if (_currency === null) {
    return amount / 100;
  }

  return Number(amount || 0).toLocaleString([], {
    style: 'currency',
    currency: _currency,
    minimumFractionDigits: _decimals
  });
}

const scrollTo = (id, yOffset = 0) => {
  if (process.client) {

    const element = document.getElementById(id)

    if (element) {
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset

      window.scrollTo({
        top: y,
        behavior: 'smooth',
      })
    } else {
      window.scrollTo({
        top: 0 + yOffset,
        behavior: 'smooth',
      })
      // alert(`Element with ID ${id} not found`)
    }
  } else {

  }
}

const optional = function (obj, param, def = null) {
  return _get(obj, param, def);
}
const get = optional;

const mediaLibraryUrl = path => {
  if (!path || path.substring(0,4) === "http") return path
  const hasNuxt = typeof $nuxt !== "undefined"
  const backend_url = hasNuxt && $nuxt?.$config?.BACKEND_URL || process.env.BACKEND_URL || ""
  return `${backend_url}/storage/app/media/${path}`
}

const sanitizeUrl = function (url) {
  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    return `https://${url}`;
  }

  return url;
}

const toast = function(message, type) {
  type = type || 'is-success';

  Toast.open({
      duration: 5000,
      message: message,
      // position: 'is-bottom',
      type: type
  })
}

export default (_, inject) => {
  const utils = {
    parseCurrency,
    scrollTo,
    slugify,
    optional,
    get,
    mediaLibraryUrl,
    toast,

  }
  // Helper for WebStorm
  Vue.prototype.$utils = utils
  inject('utils', utils)
}
