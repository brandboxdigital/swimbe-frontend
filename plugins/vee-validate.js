
import { extend, localize } from "vee-validate";
import { required, alpha, digits, email, max, min_value, confirmed } from "vee-validate/dist/rules";

import lv from 'vee-validate/dist/locale/lv.json';
import en from 'vee-validate/dist/locale/en.json';

extend('email', email);
extend('required', required);
extend('max', max);
extend('min_value', min_value);

extend('confirmed', {
  ...confirmed,
  message: "Parolēm jābūt vienādām"
});

extend('password', {
  params: ['target'],
  validate(value, { target }) {
    return value === target;
  },
  message: 'Password confirmation does not match'
});

// extend("required", {
//   ...required,
//   message: "ir obligāts"
// });

extend("alpha", {
  ...alpha,
  message: "This field must only contain alphabetic characters"
});

extend('phone', {
  validate: value => {
    return /^\+?[0-9]*\s?[0-9]*$/.test(value) && value.length > 7;
  },
  message: "Kļūdains telefona numurs. Numurs sastāv no skaitļiem.",
});

extend('agree_to_rules', {
  validate: value => {
    return value === true;
  },
  message: "formvalidate.agree_to_rules",
});

extend('agree_to_gdpr', {
  validate: value => {
    return value === true;
  },
  message: "formvalidate.gdpr",
});

extend('gift-card-value', {
  validate: value => {
    return value > 0;
  },

  message: "Vērtība nevar būt 0",
});

localize('lv', lv);
