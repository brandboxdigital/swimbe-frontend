import axios from "axios"

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Swimbe',

    htmlAttrs: {
      lang: 'lv'
    },

    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'verify-paysera', content: 'd607197bdf1e8e1a2c112c4f633d918b' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: '//cdn.cookie-script.com/s/df6a8e60891e1afcc06929d95d2259ec.js',
        tagPosition: 'bodyClose'
      }
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/utils.js' },
    { src: '~/plugins/vee-validate.js' },
    '~/plugins/directives.js',
    '~/plugins/imagor.js',
    { src: '~/plugins/3dcarousel.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  publicRuntimeConfig: {
    BACKEND_URL: process.env.BACKEND_URL || 'https://cms.swimbe.lv/'
  },

  privateRuntimeConfig: {},

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/auth-next',
    '@nuxtjs/i18n',
    '@nuxtjs/axios',
    '@nuxtjs/dayjs',
    '@nuxtjs/sitemap',
    ['nuxt-buefy', { css: false }],
    ['nuxt-facebook-pixel-module', {
      track: 'PageView',
      pixelId: '398835381154466',
      autoPageView: true,
      disabled: false
    }],
    ['@nuxtjs/gtm', {
      id: 'G-MPZW1WK90T'
    }],
  ],

  axios: {
    // baseURL: process.env.BACKEND_URL || 'https://cms.swimbe.lv/',
    credentials: true,
    withCredentials: true,

    proxy: true,

    // headers: {
    //   'Access-Control-Allow-Credentials': true,
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    // }
  },

  // https://www.npmjs.com/package/@nuxtjs/dayjs
  dayjs: {
    defaultTimeZone: 'Latvia/Riga',
    plugins: [
      'utc',
      'timezone',
      'isoWeek', // https://day.js.org/docs/en/plugin/iso-week
      'isBetween', // https://day.js.org/docs/en/plugin/is-between
      'advancedFormat', // https://day.js.org/docs/en/plugin/advanced-format
      'customParseFormat', // https://day.js.org/docs/en/plugin/custom-parse-format
    ]
  },

  i18n: {
    defaultLocale: 'lv',
    strategy: 'prefix',
    // baseURL: process.env.BACKEND_URL || 'https://cms.swimbe.lv/',
    locales: [
      { code: 'lv', file: 'get.js', name: 'Latviešu', iso: 'lv-LV', flag: 'lv' },
      // { code: 'en', file: 'get.js', name: 'English', iso: 'en-GB', flag: 'gb' },
      // @by Guntis
      // This is an Example how we will use language strings. See file ~/lang/get.js
      // { code: 'ru', file: 'get.js', name:'RU', iso: 'ru-RU' },
    ],

    detectBrowserLanguage: false,

    lazy: true,
    langDir: 'lang/',
    vueI18n: {
      silentTranslationWarn: true
    },
  },

  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },

    strategies: {
      'main': {
        provider: 'laravel/jwt',
        url: '/',
        // url: process.env.API_URL || 'https://cms.swimbe.lv/',
        endpoints: {
          login: { url: 'api/login', method: 'post' },
          logout: { url: 'api/logout', method: 'post' },
          user: { url: 'api/user', method: 'get' },
          refresh: { url: 'api/refresh', method: 'post' }
        },
        token: {
          property: 'token',
          maxAge: 60 * 60,
          global: true,
        },
        refreshToken: {
          maxAge: 20160 * 60
        },
      },
    },

    plugins: ['@/plugins/auth-lang-redirect.js']
  },

  proxy: {
    '/api/': {
      target: process.env.API_URL || 'https://cms.swimbe.lv/',
      // target: 'https://cms.swimbe.lv/',
      // pathRewrite: { '^/api/': '' }
    }
  },

  generate: {
    // Interval in milliseconds between two render cycles to avoid
    // flooding a potential API with calls from the web application.
    interval: 100,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      // "vue-intersect",
      "vee-validate/dist/rules",
      "defu", // bugfix for @nuxt/auth-next: https://stackoverflow.com/a/74463334
    ],
    postcss: null,
  },
  server: {
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 3000,
  },
  // Sitemaps (https://sitemap.nuxtjs.org/)
  sitemap: {
    /* defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
    }, */
    i18n: true,
    gzip: true,
    hostname: 'https://swimbe.lv',
    routes: async () => {
      // const target = process.env.API_URL || 'https://cms.swimbe.lv/';
      const url    = `https://cms.swimbe.lv/api/v1/products/all`;

      const { data } = await axios(url)
        .then(res => res.data)
      return data.map((product) => `/products/${product.slug}`)
    }
  },
  ssr: false,
}
