/**
 * Get all strings from backend
 */
export default async ({ $axios }, locale = 'lv') => {
  $axios.setHeader('Accept-Language', locale)
  return await $axios.$get('/api/v1/content/static-strings', {
    headers: {
      'Accept-Language': locale
    }
  })
    .then(res => res.data)
    .catch((e) => {
      console.error(`💥 Error:`, e)
    })    
}
