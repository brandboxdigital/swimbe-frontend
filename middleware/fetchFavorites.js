export default async function ({ store }) {
  if (!store.$auth.loggedIn) return

  const noFavorites = store.getters['favorite/getFavorites'].length === 0
  if (noFavorites) {
    await store.dispatch('favorite/fetchFavorites')
  }
}