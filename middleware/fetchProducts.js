export default async function ({ store }) {
  const noProducts = store.getters['products/getProducts'].length === 0
  if (noProducts) {
    await store.dispatch('products/fetchProducts')
  }
}